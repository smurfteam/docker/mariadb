# changelog for the smurfteam/docker/mariadb's project

## [0.2] - 2019-03-09
### Adds
- add mysql client
- set default root password
- set root access from localhost

## [0.1] - 2019-01-26
### Adds
- start mariadb process
- initial project

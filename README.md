# Simply and optimized MariaDB
* from: alpine linux
* author: Julien Buonocore <julien.buonocore@gmail.com>
* docker's repository: blacksmurf/mariadb

## Deployment

### Linux install procedure (for production or development platform)

#### build your image (optional)
    docker build \
    -t mariadb \
    --build-arg HOME=<container_install_directory> \
    .

#### instantiate a container from our image
    docker run \
    --detach \
    --restart always \
    --publish 3306:3306 \
    --volume <local_mount_mariadb>/mariadb:<container_install_directory>/mariadb \
    --name <my_container_name> \
    mariadb

### Windows install procedure (for development platform and Docker Desktop only)

#### build image on your desktop (necessary)
    docker build -t mariadb --build-arg install_home=<container_install_directory> --build-arg with_samba=1 .

#### create local named volumes
    docker volume create mariadb

#### create local network 192.168.100.0/24
    docker network create --subnet=192.168.100.0/24 localnetwork

#### instanciate a container with previous volumes, network and your desktop image
    docker run --detach --net=localnetwork --ip=192.168.100.3 -e USERID=0 -e GROUPID=0 --mount source=mariadb,target=<container_install_directory>/mariadb --name <your_container_name> mariadb

#### activate the TCP route with this command line and cmd.exe tool (require administrator rights) :
    route add 192.168.100.0 mask 255.255.255.0 <docker_desktop_ip_gateway> -p

## Exploitation

### Connect to the container in real-time
    docker exec -it <your_container_name> /bin/bash

### Remove all unused data (dangling images, stopped containers)
    docker system prune

### Remove unused volumes
    docker volume prune

### Remove unused networks
    docker network prune

### Access shared working folders on Windows
    \\192.169.100.3

### Connect to Hyper-V MobyLinux (for Docker for Windows)
    docker run --privileged -it --rm -v /var/run/docker.sock:/var/run/docker.sock jongallant/ubuntu-docker-client
    docker run --net=host --ipc=host --uts=host --pid=host -it --security-opt=seccomp=unconfined --privileged --rm -v /:/host alpine /bin/sh
    chroot /host

### Connect to the container in real-time
    docker exec -it <your_container_name> /bin/sh

### Secure MySQL access
    docker exec -it <your_container_name> mysql_secure_installation

### Create new SQL access
    docker exec -it <your_container_name> exploit.sh newAccess